
pub mod registers {
    use sparrow_raw_peripherals::RawPeripheral;
    use sparrow_autolib::build_register_lib;

    build_register_lib!("example_maps/test_registers.yaml", "PackedU32",
                        "sparrow_autolib_examples::registers");
}

pub mod registers16 {
    use sparrow_raw_peripherals::RawPeripheral;
    use sparrow_autolib::build_register_lib;

    build_register_lib!("example_maps/test_registers16.yaml", "PackedU16",
                        "sparrow_autolib_examples::registers");
}
