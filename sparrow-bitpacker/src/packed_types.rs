use typenum;
use typenum::consts::*;
use typenum::{Less, Cmp, Sum};

use core::ops::{BitAnd, BitOr, BitXor, Add, Shl, Shr, Not};
use core::cmp::{PartialEq, PartialOrd};
use num;
use num::{NumCast, Zero, One};
use core::marker::PhantomData;

/// Trait to endow foreign types with type-level length information.
///
/// This is marked unsafe as setting the length value incorrectly can result
/// in undefined behaviour.
pub unsafe trait TypeLength {
    type Length: typenum::Unsigned + Add<U1> + PartialEq;
}

unsafe impl TypeLength for u128 {
    type Length = U128;
}

unsafe impl TypeLength for u64 {
    type Length = U64;
}

unsafe impl TypeLength for u32 {
    type Length = U32;
}

unsafe impl TypeLength for u16 {
    type Length = U16;
}

unsafe impl TypeLength for u8 {
    type Length = U8;
}

/// Trait for representing the packed type that is used by the bitpacker.
/// It encapsulates both the target type for a packed field and also the
/// length of the packed field, which need not necessarily completely fill
/// the target type.
///
/// The packed field _does_ need to fit within the packed type, and this is
/// enforced through the [TypeLength] extension trait. This is correctly
/// implemented for the standard types, though it is possible to break things
/// by misdefining concrete implementations of that trait.
///
pub trait PackedType {
    /// The traits for Type are determined by the requirements of Sparrow, but
    /// they should be a generally useful collection.
    type Type: num::Unsigned + PartialEq + PartialOrd + Copy +
        BitAnd<Output = Self::Type> + BitOr<Output = Self::Type> +
        BitXor<Output = Self::Type> + Not<Output = Self::Type> +
        Shl<Output = Self::Type> + Shr<Output = Self::Type> +
        core::fmt::Debug + TypeLength + NumCast + Zero + One;
    type PackedLength: typenum::Unsigned + Add<U1> + PartialEq +
        Cmp<Sum<<<Self as PackedType>::Type as TypeLength>::Length, U1>, Output=Less>;

}

#[derive(Debug, PartialEq, Copy, Clone)]
/// A concrete implementation of a packed type that packs into a complete
/// unsigned integer type. That is, the length of the packed field is the same
/// as the size of the unsigned integer.
pub struct PackedFullUint<T>
where T: num::Unsigned + PartialEq + PartialOrd + Copy +
BitAnd<Output = T> + BitOr<Output = T> + BitXor<Output = T> + Not<Output = T> +
Shl<Output = T> + Shr<Output = T> +
core::fmt::Debug + TypeLength + NumCast + Zero + One
{
    marker_t: PhantomData<T>
}

impl <T> PackedType for PackedFullUint<T>
where T: num::Unsigned + PartialEq + PartialOrd + Copy +
          BitAnd<Output = T> + BitOr<Output = T> + 
          BitXor<Output = T> + Not<Output = T> +
          Shl<Output = T> + Shr<Output = T> +
          core::fmt::Debug + TypeLength + NumCast + Zero + One,
      <T as TypeLength>::Length: typenum::Unsigned + Add<U1> +
          Cmp<Sum<<T as TypeLength>::Length, U1>, Output=Less>
{
    type Type = T;
    type PackedLength = <<Self as PackedType>::Type as TypeLength>::Length;
}

/// Type alias to `PackedFullUint<u8>`.
pub type PackedU8 = PackedFullUint<u8>;
/// Type alias to `PackedFullUint<u16>`.
pub type PackedU16 = PackedFullUint<u16>;
/// Type alias to `PackedFullUint<u32>`.
pub type PackedU32 = PackedFullUint<u32>;
/// Type alias to `PackedFullUint<u64>`.
pub type PackedU64 = PackedFullUint<u64>;
/// Type alias to `PackedFullUint<u128>`.
pub type PackedU128 = PackedFullUint<u128>;


#[cfg(test)]
mod packed_u24_tests {
    //! It should be possible to set a packed size with is less than the 
    //! length of the target type.
    //!
    use super::PackedType;
    use typenum::Unsigned;
    use typenum::consts::U24;

    struct PackedU24 {}

    impl PackedType for PackedU24 {
        type Type = u32;
        type PackedLength = U24;
    }

    #[test]
    /// The PackedLength should resolve to a size which is less than or equal
    /// to the length of Type
    fn test_lengths() {
        use core::mem::size_of;
        const _BYTE_BITS: usize = 8;

        assert!(
            size_of::<<PackedU24 as PackedType>::Type>() * _BYTE_BITS == 32);

        assert!(<PackedU24 as PackedType>::PackedLength::to_usize() == 24);
    }
}

#[cfg(test)]
mod packed_u128_tests {
    //! The PackedX types should be constrained to be self consistent.
    use super::PackedU128 as PackedX;
    use super::PackedType;
    use typenum::Unsigned;

    #[test]
    /// The PackedLength should resolve to a size which is equal
    /// to the length of Type
    fn test_lengths() {
        use core::mem::size_of;
        const _BYTE_BITS: usize = 8;

        assert!(
            size_of::<<PackedX as PackedType>::Type>() * _BYTE_BITS ==
            <PackedX as PackedType>::PackedLength::to_usize())
    }
}

#[cfg(test)]
mod packed_u64_tests {
    //! The PackedX types should be constrained to be self consistent.
    use super::PackedU64 as PackedX;
    use super::PackedType;
    use typenum::Unsigned;

    #[test]
    /// The PackedLength should resolve to a size which is equal
    /// to the length of Type
    fn test_lengths() {
        use core::mem::size_of;
        const _BYTE_BITS: usize = 8;

        assert!(
            size_of::<<PackedX as PackedType>::Type>() * _BYTE_BITS ==
            <PackedX as PackedType>::PackedLength::to_usize())
    }
}

#[cfg(test)]
mod packed_u32_tests {
    //! The PackedX types should be constrained to be self consistent.
    use super::PackedU32 as PackedX;
    use super::PackedType;
    use typenum::Unsigned;

    #[test]
    /// The PackedLength should resolve to a size which is equal
    /// to the length of Type
    fn test_lengths() {
        use core::mem::size_of;
        const _BYTE_BITS: usize = 8;

        assert!(
            size_of::<<PackedX as PackedType>::Type>() * _BYTE_BITS ==
            <PackedX as PackedType>::PackedLength::to_usize())
    }
}


#[cfg(test)]
mod packed_u16_tests {
    //! The PackedX types should be constrained to be self consistent.
    use super::PackedU16 as PackedX;
    use super::PackedType;
    use typenum::Unsigned;

    #[test]
    /// The PackedLength should resolve to a size which is equal
    /// to the length of Type
    fn test_lengths() {
        use core::mem::size_of;
        const _BYTE_BITS: usize = 8;

        assert!(
            size_of::<<PackedX as PackedType>::Type>() * _BYTE_BITS ==
            <PackedX as PackedType>::PackedLength::to_usize())
    }
}

#[cfg(test)]
mod packed_u8_tests {
    //! The PackedX types should be constrained to be self consistent.
    use super::PackedU8 as PackedX;
    use super::PackedType;
    use typenum::Unsigned;

    #[test]
    /// The PackedLength should resolve to a size which is equal
    /// to the length of Type
    fn test_lengths() {
        use core::mem::size_of;
        const _BYTE_BITS: usize = 8;

        assert!(
            size_of::<<PackedX as PackedType>::Type>() * _BYTE_BITS ==
            <PackedX as PackedType>::PackedLength::to_usize())
    }
}
