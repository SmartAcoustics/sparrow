use typenum::consts::*;

use sparrow_bitpacker::PackedType;

/// A type with a too-small target type
struct BrokenPackedU17 {}

impl PackedType for BrokenPackedU17 {
    type Type = u16;
    type PackedLength = U17;
}

fn main() -> () {
}
