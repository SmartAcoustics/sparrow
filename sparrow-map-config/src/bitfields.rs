use strict_yaml_rust::StrictYaml;
use snafu::{Snafu, ResultExt};

#[derive(Debug, PartialEq, Snafu)]
pub enum BitfieldParseError {
    #[snafu(display("Cannot parse the yaml value for \"{}\": \"{}\"; {}", key, val, source))]
    ParsingBitfieldYamlU32 { key: String, val: String, source: std::num::ParseIntError },
    #[snafu(display("Cannot parse the yaml value for \"{}\": \"{}\"; {}", key, val, source))]
    ParsingBitfieldYamlBool { key: String, val: String, source: std::str::ParseBoolError },
    #[snafu(display("Default {} is too large. It must be smaller than {}", default_val, max_val))]
    U32DefaultOutOfRange { default_val: u32, max_val: u32 },
    #[snafu(display("Missing bitfield key: \"{}\"", key))]
    MissingBitfieldYamlKey { key: String },
    #[snafu(display("Unknown bitfield type: \"{}\"", type_str))]
    UnknownBitfieldType { type_str: String },
}

pub type BitfieldParseResult<T, E = BitfieldParseError> = std::result::Result<T, E>;

#[derive(Debug, PartialEq)]
pub struct BoolBitfieldDef {
    pub description: Option<String>,
    pub offset: u32,
    pub default: Option<bool>,
}

#[derive(Debug, PartialEq)]
pub struct UintBitfieldDef {
    pub description: Option<String>,
    pub offset: u32,
    pub length: u32,
    pub default: Option<u32>,
}

#[derive(Debug, PartialEq)]
pub struct UintSha1HashBitfieldDef {
    pub uint_bitfield: UintBitfieldDef,
    pub digest_offset: u32,
    pub digest_digits: u32,
}

#[derive(Debug, PartialEq)]
pub enum BitfieldTypes {
    Bool(BoolBitfieldDef),
    Uint(UintBitfieldDef),
    UintSha1Hash(UintSha1HashBitfieldDef),
}

impl BitfieldTypes {
    pub(crate) fn from_strictyaml(bitfield_config: &StrictYaml) -> BitfieldParseResult<BitfieldTypes> {

        let bitfield_type_str = bitfield_config["type"].as_str().ok_or(
            BitfieldParseError::MissingBitfieldYamlKey { key: "type".to_string()} )?;

        // Map d to a String from a &str
        let description = bitfield_config["description"].as_str().map(
            |d| d.to_string());

        match bitfield_type_str.as_ref() {
            "bool" => {
                let offset_str = bitfield_config["offset"].as_str().ok_or(
                    BitfieldParseError::MissingBitfieldYamlKey { key: "offset".to_string()} )?;

                let offset: u32 = offset_str.parse().context(
                    ParsingBitfieldYamlU32 {
                        key: "offset".to_string(),
                        val: offset_str.to_string() } )?;

                let default_opt = bitfield_config["default"].as_str();

                let default = match default_opt {
                    Some(val) => Some(
                        val.parse::<bool>().context(
                            ParsingBitfieldYamlBool {
                                key: "offset".to_string(),
                                val: val.to_string() } )?),
                    None => None
                };

                Ok(BitfieldTypes::Bool(
                        BoolBitfieldDef{offset, default, description}))

            },
            "uint" => {
                let offset_str = bitfield_config["offset"].as_str().ok_or(
                    BitfieldParseError::MissingBitfieldYamlKey { key: "offset".to_string()} )?;
                let length_str = bitfield_config["length"].as_str().ok_or(
                    BitfieldParseError::MissingBitfieldYamlKey { key: "length".to_string()} )?;

                let offset: u32 = offset_str.parse().context(
                    ParsingBitfieldYamlU32 {
                        key: "offset".to_string(),
                        val: offset_str.to_string() } )?;

                let length: u32 = length_str.parse().context(
                    ParsingBitfieldYamlU32 {
                        key: "length".to_string(),
                        val: length_str.to_string() } )?;

                let default_opt = bitfield_config["default"].as_str();

                let default = match default_opt {
                    Some(val) => {
                        let u32_val = val.parse::<u32>().context(
                            ParsingBitfieldYamlU32 {
                                key: "offset".to_string(),
                                val: val.to_string() } )?;

                        if u32_val >= 2u32.pow(length) {
                            return Err(BitfieldParseError::U32DefaultOutOfRange {
                                default_val: u32_val, max_val: 2u32.pow(length)});
                        }

                        Some(u32_val)
                    },
                    None => None
                };

                Ok(BitfieldTypes::Uint(
                        UintBitfieldDef{offset, length, default, description}))
            },
            "uint-sha1-hash" => {

                const DIGEST_BITS_PER_DIGIT: u32 = 4;

                let offset_str = bitfield_config["offset"].as_str().ok_or(
                    BitfieldParseError::MissingBitfieldYamlKey {
                        key: "offset".to_string()} )?;
                let digest_digits_str = bitfield_config["digest-digits"].as_str().ok_or(
                    BitfieldParseError::MissingBitfieldYamlKey {
                        key: "digest-digits".to_string()} )?;
                let digest_offset_str = bitfield_config["digest-offset"].as_str().ok_or(
                    BitfieldParseError::MissingBitfieldYamlKey {
                        key: "digest-offset".to_string()} )?;

                let offset: u32 = offset_str.parse().context(
                    ParsingBitfieldYamlU32 {
                        key: "offset".to_string(),
                        val: offset_str.to_string() } )?;

                let digest_digits: u32 = digest_digits_str.parse().context(
                    ParsingBitfieldYamlU32 {
                        key: "digest_digits".to_string(),
                        val: digest_digits_str.to_string() } )?;

                let digest_offset: u32 = digest_offset_str.parse().context(
                    ParsingBitfieldYamlU32 {
                        key: "digest_offset".to_string(),
                        val: digest_offset_str.to_string() } )?;

                let default = None;

                let length = digest_digits * DIGEST_BITS_PER_DIGIT;

                let uint_bitfield = UintBitfieldDef{offset, length, default, description};

                Ok(BitfieldTypes::UintSha1Hash(
                        UintSha1HashBitfieldDef{uint_bitfield, digest_digits, digest_offset}))

            },
            _ => Err(BitfieldParseError::UnknownBitfieldType {
                type_str: bitfield_type_str.to_string() })
        }
    }
}

