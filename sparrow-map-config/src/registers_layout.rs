use strict_yaml_rust::StrictYaml;
use snafu::{Snafu, ResultExt};
use indexmap::IndexMap;
use std::collections::HashMap;

use crate::register::{Register, RegisterParseError};

#[derive(Debug, PartialEq, Snafu)]
pub enum LayoutParseError {
    #[snafu(display("Cannot parse the config for register \"{}\": {}", reg_name, source ))]
    RegisterError { reg_name: String, source: RegisterParseError },
    #[snafu(display("Invalid register name: {}", name))]
    InvalidRegisterName { name: String },
    #[snafu(display("Register \"{}\" has the same index as register \"{}\"", reg1, reg2))]
    RepeatedRegisterIndex { reg1: String, reg2: String },
}

pub type LayoutParseResult<T, E = LayoutParseError> = std::result::Result<T, E>;

#[derive(Debug, PartialEq)]
/// The datatype representing a layout of registers. Effectively, the mapping
/// from register name to register properties.
pub struct RegistersLayout {
    pub layout: IndexMap<String, Register>,
}

impl RegistersLayout {
    pub(crate) fn from_strictyaml(registers_yaml: &StrictYaml) -> LayoutParseResult<Self> {

        let mut registers = RegistersLayout{ layout: IndexMap::new() };

        match registers_yaml {
            StrictYaml::Hash(registers_config) => {
                for (register_name, register_config_yaml) in registers_config {
                    let register_name_str = register_name.as_str().unwrap().to_string();

                    if syn::parse_str::<syn::Ident>(&register_name_str).is_err() {
                        // We have to explicitly return here so our if statement
                        // has type ()
                        return Err(LayoutParseError::InvalidRegisterName {
                            name: register_name_str });
                    }

                    let reg_name = register_name.as_str().unwrap().to_string();
                    registers.layout.insert(
                        register_name_str,
                        Register::from_strictyaml(register_config_yaml).context(
                            RegisterError{ reg_name })?);
                }
            },
            _ => (),
        }

        // Check for repeated register indices
        let mut repeat_checker = HashMap::new();

        for (register_name, register) in &registers.layout {
            match repeat_checker.insert(register.index, register_name) {
                None => (),
                Some(prev_reg_name) => {
                    return Err(LayoutParseError::RepeatedRegisterIndex{
                        reg1: register_name.clone(), reg2: prev_reg_name.clone() });
                },
            }
        }


        Ok(registers)
    }

}
