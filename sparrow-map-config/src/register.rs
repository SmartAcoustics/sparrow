use strict_yaml_rust::StrictYaml;
use snafu::{Snafu, ResultExt};

use crate::multibitfield::{
    MultiBitfieldParseError,
    MultiBitfieldDef,};

#[derive(Debug, PartialEq, Snafu)]
pub enum RegisterParseError {
    #[snafu(display("Cannot parse the bitfields: {}", source))]
    MultiBitfieldError { source: MultiBitfieldParseError },
    #[snafu(display("Cannot parse the yaml value for \"{}\": \"{}\"; {}", key, val, source))]
    ParsingRegisterYamlU32 { key: String, val: String, source: std::num::ParseIntError },
    #[snafu(display("Missing register key: \"{}\"", key))]
    MissingRegisterYamlKey { key: String },
    #[snafu(display("Cannot parse the register type: {}", err_str))]
    RegisterTypeError { err_str: String },
}

pub type RegisterParseResult<T, E = RegisterParseError> = std::result::Result<T, E>;


#[derive(Debug, PartialEq)]
/// The type representing a register configuration.
pub struct Register {
    pub index: u32,
    pub description: Option<String>,
    pub reg_type: RegisterType,
    pub bitfields: MultiBitfieldDef,
}

impl Register {

    pub(crate) fn from_strictyaml(register_yaml: &StrictYaml) -> RegisterParseResult<Self> 
    {
        let description = register_yaml["description"].as_str().map(
            |d| d.to_string());

        let index_str = register_yaml["index"].as_str().ok_or(
            RegisterParseError::MissingRegisterYamlKey { key: "index".to_string()} )?;

        let index: u32 = index_str.parse().context(
            ParsingRegisterYamlU32 {
                key: "index".to_string(),
                val: index_str.to_string() } )?;

        let reg_type_str = register_yaml["type"].as_str().ok_or(
            RegisterParseError::MissingRegisterYamlKey { key: "type".to_string()} )?;

        let reg_type = RegisterType::from_str(reg_type_str)?;

        let bitfields = MultiBitfieldDef::from_strictyaml(
            &register_yaml["bitfields"]).context(MultiBitfieldError)?;

        Ok(Register { description, index, reg_type, bitfields } )
    }
}

#[derive(Debug, PartialEq)]
pub enum RegisterType {
    ReadOnly,
    WriteOnly,
    ReadWrite,
}

impl RegisterType {
    pub fn from_str(s: &str) -> RegisterParseResult<Self> {

        let s_upper = s.to_string().to_uppercase();

        match s_upper.as_ref() {
            "RO" => Ok(RegisterType::ReadOnly),
            "RW" => Ok(RegisterType::ReadWrite),
            "WO" => Ok(RegisterType::WriteOnly),
            _ => Err(
                RegisterParseError::RegisterTypeError{
                    err_str: format!(
                                 "Unknown register type: \"{}\" \
                                 (must be one of RO, RW or WO)", s).to_string() }),
        }
    }
}


