
use sparrow_map_config::registers_layout_from_file;

#[test]
fn test_bitfield_name_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the \
        config for register \"control\": Cannot parse the \
        bitfields: Invalid bitfield name: \"0go\"";

    match registers_layout_from_file("tests/fails/bitfield_name_fail.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_invalid_bitfield_type() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"control\": Cannot parse the bitfields: Cannot parse the \
        multi-bitfield for field \"go\": Unknown bitfield type: \"fooo\"";

    match registers_layout_from_file("tests/fails/invalid_bitfield_type.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_invalid_register_type() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"control\": Cannot parse the register type: Unknown \
        register type: \"XY\" (must be one of RO, RW or WO)";

    match registers_layout_from_file("tests/fails/invalid_register_type.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_invalid_reg_index() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"control\": Cannot parse the yaml value for \"index\": \
        \"Invalid\"; invalid digit found in string";

    match registers_layout_from_file("tests/fails/invalid_reg_index.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_missing_bitfield_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"control\": Cannot parse the bitfields: No bitfields yaml \
        keys";

    match registers_layout_from_file("tests/fails/missing_bitfield_fail.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_register_name_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Invalid register \
        name: 0control";

    match registers_layout_from_file("tests/fails/register_name_fail.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_repeated_bitfield_name_fail() {

    let err_str =
        "Error scanning the YAML document: Error handling node: Key already \
        exists in the hash map at line 14 column 1";

    match registers_layout_from_file("tests/fails/repeated_bitfield_name_fail.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_repeated_reg_index_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Register \"status\" has the \
        same index as register \"control\"";

    match registers_layout_from_file("tests/fails/repeated_reg_index_fail.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_repeated_reg_name_fail() {

    let err_str =
        "Error scanning the YAML document: Error handling node: \
        Key already exists in the hash map at line 20 column 1";

    match registers_layout_from_file("tests/fails/repeated_reg_name_fail.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_invalid_uint_default_bitfield_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"control\": Cannot parse the bitfields: Cannot parse the \
        multi-bitfield for field \"go\": Cannot parse the yaml value for \
        \"offset\": \"something_wrong\"; invalid digit found in string";

    match registers_layout_from_file("tests/fails/invalid_uint_default_bitfield.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_invalid_bool_default_bitfield_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"control\": Cannot parse the bitfields: Cannot parse the \
        multi-bitfield for field \"go\": Cannot parse the yaml value for \
        \"offset\": \"thisisnotright\"; provided string was not `true` or `false`";

    match registers_layout_from_file("tests/fails/invalid_bool_default_bitfield.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_bool_no_offset_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"control\": Cannot parse the bitfields: Cannot parse the \
        multi-bitfield for field \"go\": Missing bitfield key: \"offset\"";

    match registers_layout_from_file(
        "tests/fails/bitfield_uint_no_offset.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_uint_no_offset_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"control\": Cannot parse the bitfields: Cannot parse the \
        multi-bitfield for field \"go\": Missing bitfield key: \"offset\"";

    match registers_layout_from_file(
        "tests/fails/bitfield_uint_no_offset.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_uint_no_length_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"control\": Cannot parse the bitfields: Cannot parse the \
        multi-bitfield for field \"go\": Missing bitfield key: \"length\"";

    match registers_layout_from_file(
        "tests/fails/bitfield_uint_no_length.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_uint_too_large_default_bitfield_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"control\": Cannot parse the bitfields: Cannot parse the \
        multi-bitfield for field \"go\": Default 1024 is too large. It must \
        be smaller than 1024";

    match registers_layout_from_file(
        "tests/fails/bitfield_uint_default_too_large.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_uint_sha1_hash_no_offset_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"hash\": Cannot parse the bitfields: Cannot parse the \
        multi-bitfield for field \"word\": Missing bitfield key: \"offset\"";

    match registers_layout_from_file(
        "tests/fails/bitfield_uint_sha1_hash_no_offset.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_uint_sha1_hash_no_digest_offset_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"hash\": Cannot parse the bitfields: Cannot parse the \
        multi-bitfield for field \"word\": Missing bitfield key: \"digest-offset\"";

    match registers_layout_from_file(
        "tests/fails/bitfield_uint_sha1_hash_no_digest_offset.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}

#[test]
fn test_uint_sha1_hash_no_digest_length_fail() {

    let err_str =
        "Error parsing the YAML to our schema: Cannot parse the config for \
        register \"hash\": Cannot parse the bitfields: Cannot parse the \
        multi-bitfield for field \"word\": Missing bitfield key: \"digest-digits\"";

    match registers_layout_from_file(
        "tests/fails/bitfield_uint_sha1_hash_no_digest_length.yaml") {
        Ok(_) => panic!(),
        Err(e) => assert_eq!(err_str, format!("{}", e))
    }
}
