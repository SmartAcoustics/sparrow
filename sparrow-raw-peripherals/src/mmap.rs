use std::fs::OpenOptions;
use std::path::Path;
use std::ptr::{write_volatile, read_volatile};
use std::sync::Arc;
use parking_lot::Mutex;
use std::convert::TryFrom;
use std::marker::PhantomData;

use memmap::{MmapOptions, MmapMut};
use snafu::ResultExt;

use crate::{
    RawPeripheral,
    RawPeripheralWord,
    PeripheralResult,
};

use crate::errors::{
    IndexConversionError,
    DeviceConfigurationError,
    DeviceAccessError};

use sparrow_bitpacker::{PackedType, PackedU32};

/// A memory-mapped peripheral device. This is initialised through
pub struct MmapPeripheral <T = PackedU32>
where T: PackedType,
{
    mmap: Arc<Mutex<MmapMut>>,
    t_marker: PhantomData<T>,
}

impl <T> MmapPeripheral <T>
where T: PackedType,
{

    pub fn new(file_path: &Path, size: usize) -> PeripheralResult<Self> {

        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(file_path).context(DeviceAccessError)?;

        let mmap = unsafe {
            MmapOptions::new()
                .offset(0)
                .len(size)
                .map_mut(&file)
                .context(DeviceConfigurationError)?
        };

        let mmap = Arc::new(Mutex::new(mmap));

        Ok(MmapPeripheral{mmap, t_marker: PhantomData })
    }
}

impl <T> RawPeripheral<T> for MmapPeripheral <T>
where T: PackedType,
{

    fn write(&self, index: usize, value: T::Type) -> PeripheralResult<()> {
        let index = isize::try_from(index).context(IndexConversionError)?;

        {
            let mem_ptr = self.mmap.lock().as_mut_ptr() as *mut T::Type;

            unsafe {
                write_volatile(mem_ptr.offset(index), value);
            }

            Ok(())
        }
    }

    fn read(&self, index: usize) -> PeripheralResult<T::Type> {
        let index = isize::try_from(index).context(IndexConversionError)?;

        let read_data: T::Type;
        {
            let mem_ptr = self.mmap.lock().as_mut_ptr() as *mut T::Type;

            unsafe {
                read_data = read_volatile(mem_ptr.offset(index));
            }
        }

        Ok(read_data)
    }

    fn to_word<IDX: typenum::Unsigned>(&self) -> RawPeripheralWord<Self, IDX, T> {

        let mmap = Arc::clone(&self.mmap);

        let rc_peripheral = MmapPeripheral {
            mmap, t_marker: PhantomData};

        RawPeripheralWord::<Self, IDX, T>::new(rc_peripheral)
    }
}

#[cfg(test)]
mod mmap_tests {
    //! The MMAP raw peripheral should provide a low level, safe representation
    //! of an memory-mapped file.
    use super::*;
    use sparrow_bitpacker::PackedU32;
    use typenum::consts::*;

    #[test]
    fn test_mmap_normal_read_write() {
        use tempfile::NamedTempFile;

        let mmap_file = NamedTempFile::new().unwrap();
        let file_len = 100u8; // always convertible to u64 and usize

        {
            // Create the file and set it to the right size
            let file = OpenOptions::new()
                .read(true)
                .write(true)
                .open(mmap_file.path()).unwrap();

            file.set_len(file_len as u64).unwrap();
        }

        let mmap_peripheral: MmapPeripheral<PackedU32> =
            MmapPeripheral::new(mmap_file.path(), file_len as usize).unwrap();

        let register_0 = mmap_peripheral.to_word::<U0>();
        let register_10 = mmap_peripheral.to_word::<U10>();

        register_0.write(0x10).unwrap();
        register_10.write(0x11).unwrap();

        println!("{}", register_0.read().unwrap());
        println!("{}", register_10.read().unwrap());
    }
}
