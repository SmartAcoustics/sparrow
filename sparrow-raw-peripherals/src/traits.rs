use typenum::Unsigned;
use sparrow_bitpacker::{PackedType, PackedU32};

use crate::errors::PeripheralResult;
use super::RawPeripheralWord;

pub trait RawPeripheral<T = PackedU32>: Sized
where T: PackedType
{
    fn write(&self, index: usize, value: T::Type) -> PeripheralResult<()>;
    fn read(&self, index: usize) -> PeripheralResult<T::Type>;
    fn to_word<IDX: Unsigned>(&self) -> RawPeripheralWord<Self, IDX, T>;
}
