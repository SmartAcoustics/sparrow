use snafu::Snafu;
use std::io;
use std::num::TryFromIntError;

#[derive(Debug, Snafu)]
#[snafu(visibility = "pub")]
pub enum PeripheralError {
    #[snafu(display("Error converting index to a valid type: {}", source))]
    IndexConversionError {source: TryFromIntError},
    #[snafu(display("Error acquiring the mutex lock"))]
    MutexAcquisitionError,
    #[snafu(display("Error accessing the device file: {}", source))]
    DeviceAccessError {source: io::Error},
    #[snafu(display("Error configuring the device: {}", source))]
    DeviceConfigurationError {source: io::Error},
    #[snafu(display("Error reading from peripheral: {}", source))]
    ReadError { source: io::Error },
    #[snafu(display("Error writing to peripheral: {}", source))]
    WriteError { source: io::Error },
}

pub type PeripheralResult<T, E = PeripheralError> = std::result::Result<T, E>;
