extern crate proc_macro;

use quote::quote;
use proc_macro::TokenStream;
use syn::parse::{ParseStream, Parse, Result};
use syn;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std;
use regex;
use crypto::{digest::Digest, sha1::Sha1};
use std::collections::HashMap;

use sparrow_map_config::registers_layout_from_file;

pub(crate) mod to_tokens;
mod bitfields;
mod multibitfield;
mod register;
mod registers_layout;

use to_tokens::ToTokens;

#[derive(Debug)]
struct BuilderInput {
    filename: String,
    packed_type_str: String,
    namespace_str: String,
}

impl Parse for BuilderInput {

    fn parse(input: ParseStream) -> Result<Self> {
        let lit_filename: syn::LitStr = input.parse()?;
        let filename: String = lit_filename.value();

        let lookahead = input.lookahead1();
        let packed_type_str = if lookahead.peek(syn::Token![,]) {
            input.parse::<syn::Token![,]>()?;
            let lit_namespace: syn::LitStr = input.parse()?;
            lit_namespace.value()
        } else {
            "PackedU32".to_string()
        };

        let lookahead = input.lookahead1();
        let namespace_str = if lookahead.peek(syn::Token![,]) {
            input.parse::<syn::Token![,]>()?;
            let lit_namespace: syn::LitStr = input.parse()?;
            lit_namespace.value()
        } else {
            "".to_string()
        };

        Ok(Self { filename, packed_type_str, namespace_str })
    }
}

#[proc_macro]
/// Builds a register layout library from the provided StrictYAML file, 
/// building on other parts of sparrow - notably the register system and the
/// bitpacker.
///
/// The first argument should be a string of the filename, either a path
/// relative to the crate root or an absolute path.
///
/// An optional second argument is a string giving the namespace of _this_ invocation. It is used
/// when populating the docstrings and allows properly functioning doctests. Aside from the
/// documentation aspects, setting or not this argument does not affect the functionality of the
/// library.
///
/// The generated library will be documented properly as part of the crate
/// from which it was called and it is this documentation that describes
/// the full usage.
///
/// The YAML document can be found as per the path in the example.
///```
/// use sparrow_bitpacker::Bitfield;
/// use sparrow_registers::registers::{ReadableRegister, WriteableRegister};
/// use sparrow_raw_peripherals::mock::{MockPeripheral, MemoryHandler};
/// use sparrow_raw_peripherals::RawPeripheral;
///
/// mod registers {
///     use sparrow_autolib::build_register_lib;
///     use sparrow_raw_peripherals::RawPeripheral;
///
///     // If the crate we are in is called mycrate, then we can optionally
///     // provide two additional arguments. The first is the underlying
///     // packed type, which comes from sparrow-bitpacker, and currently
///     // needs to be one of PackedU32 or PackedU16 (defaulting to PackedU32
///     // if it is missing). It determines the 
///     // underlying packed type of the register system. The second
///     // specifies a namespace of the generated lib with mycrate::registers.
///     // This is used so the documentation is correct, but is not necessary
///     // for the generated library to work properly.
///     build_register_lib!("tests/test_registers.yaml", "PackedU32", "mycrate::registers");
///     //build_register_lib!("tests/test_registers.yaml"); // <- Also fine
/// }
///
/// fn main() {
///     // We use the `fields` api to access the individual fields
///     // We'd normally handle the returns properly, but difficult in a
///     // doctest!
///     let mut control_bf = registers::control::fields::go(true).unwrap();
///     control_bf.update(&registers::control::fields::length(10).unwrap());
///
///     let config_bf = registers::config::fields::power(15).unwrap();
///
///     // The go field is the zeroth bit, length is the offset by 2 bits
///     //assert_eq!(control_bf.pack().unwrap(), (1 << 0) + (10 << 2));
///     
///     // We need to remember the default value when we check the output
///     let default_size =
///         registers::config::Bitfields::default().size.unwrap().pack().unwrap();
///     let default_frobinate =
///         registers::config::Bitfields::default().frobinate.unwrap().pack().unwrap();
///
///     // power is offset by 6 bits
///     assert_eq!(
///         config_bf.pack().unwrap(),
///         15 << 6 | default_size | default_frobinate);
///
///     // Now we do something with the registers...
///
///     // We use a mock peripheral for demonstration purposes
///     let handler = MemoryHandler::new();
///     let p = MockPeripheral::new(handler);
///
///     let mut reg_layout = registers::RegistersLayout::new(p);
///
///     reg_layout.control.write(control_bf).unwrap();
///
///     // control is the zeroth register and is write-only, so we need to
///     // check it by inspecting the raw peripheral (which is something we
///     // can do with a MockPeripheral).
///     assert_eq!(
///         reg_layout.raw_peripheral.read(0).unwrap(),
///         control_bf.pack().unwrap());
///
///     // We write then read from the RW register.
///     reg_layout.config.write(config_bf).unwrap();
///     let config_value = reg_layout.config.read().unwrap();
///     
///     // We convert the bitfields into `ExtractedBitfields` as this properly
///     // populates the default values when they are not set and allows a
///     // proper comparison (read value are always fully populate since the
///     // the read word always contains a valid value).
///     let read: registers::config::ExtractedBitfields = config_value.into();
///     let expected: registers::config::ExtractedBitfields = config_bf.into();
///     assert_eq!(read, expected);
/// }
///```
pub fn build_register_lib(input: TokenStream) -> TokenStream {

    let input = syn::parse_macro_input!(input as BuilderInput);

    let register_length_str = match input.packed_type_str.as_str() {
        "PackedU16" => "16".to_string(),
        "PackedU32" => "32".to_string(),
        _ => panic!(
            "The type string, if set, must be one of PackedU16 or PackedU32")
    };

    let filename = Path::new(&input.filename);

    let file_path = if filename.is_absolute() {
        filename.to_path_buf()

    } else {

        let cwd = match std::env::current_dir() {
            Ok(val) => val,
            Err(_) => panic!("The current working directory is not defined properly")
        };

        let cargo_path = cwd.join("Cargo.toml");
        let mut cargo_file = match File::open(cargo_path) {
            Ok(val) => val,
            Err(_) => panic!("Cannot open the root cargo file")
        };

        let mut cargo_contents = String::new();
        cargo_file.read_to_string(&mut cargo_contents).unwrap();

        let cargo_re = regex::Regex::new(r"(?m)^\[workspace\][ \t]*$").unwrap();

        let workspace_path = match cargo_re.find(&cargo_contents) {
            Some(_) => std::env::var("CARGO_PKG_NAME").unwrap(),
            None => "".to_string()
        };

        cwd.join(workspace_path).join(input.filename)
    };

    let file_path_str = format!("{}", file_path.display());
    let register_map = match registers_layout_from_file(&file_path_str) {
        Ok(layout) => layout,
        Err(e) => panic!("{}", e)
    };

    let yaml_string = match std::fs::read_to_string(&file_path_str) {
        Ok(yaml_string) => yaml_string,
        Err(e) => panic!("{}", e)
    };

    // We write out the hex digest as well.
    let mut hasher = Sha1::new();
    hasher.input_str(&yaml_string);
    let yaml_hex_digest = hasher.result_str();

    let mut context = HashMap::new();
    context.insert("namespace".to_string(), input.namespace_str.clone());
    context.insert("packed_type".to_string(), input.packed_type_str.clone());
    context.insert("yaml_sha1_hash".to_string(), yaml_hex_digest.clone());
    context.insert("register_length".to_string(), register_length_str.clone());

    let register_tokens = register_map.to_tokens(&context);

    let result = quote!(
        const _YAML_FILE_STR: &'static str = include_str!(#file_path_str);
        /// The SHA-1 40-byte hex digest of the string read from the YAML file.
        pub const YAML_SHA1_DIGEST: &'static str = #yaml_hex_digest;
        #register_tokens
        );

    result.into()
}

