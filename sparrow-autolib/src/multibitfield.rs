use quote::{quote, format_ident};
use proc_macro2::{TokenStream as TokenStream2};
use std::collections::{HashMap, HashSet};
use sparrow_map_config::{
    multibitfield::MultiBitfieldDef,
    bitfields::BitfieldTypes};

use crate::{
    to_tokens::ToTokens,
    bitfields::BitfieldTokenOutputs};

// We can add the context to each of these functions as necessary - most of
// them were written before it was available and most don't need it.
trait BitfieldTokenChunks {
    fn bitfields_type_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn bitpacker_use_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn fn_new_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn doctest_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn fn_pack_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn fn_unpack_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn fn_update_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn bitfield_defaults_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn extracted_bitfields_type_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn fn_extracted_from_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn mod_field_constructors_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn extra_bitfield_methods_tokens(&self, context: &HashMap<String, String>)
        -> TokenStream2;
    fn check_context(&self, context: &HashMap<String, String>);
    fn check_bitfields_valid(&self, context: &HashMap<String, String>);
}

impl BitfieldTokenChunks for MultiBitfieldDef {
    /// Declares the multi-bitfield type. Expands to something like:
    /// ```ignore
    /// #[derive(Debug, PartialEq, Copy, Clone)]
    /// pub struct Bitfields {
    ///     pub go: Option<BoolBitfield<U0>>,
    ///     pub stop: Option<BoolBitfield<U1>>,
    ///     pub length: Option<UintBitfield<U2, U10>>,
    /// }
    /// ```
    /// (though of course the derive attributes are further expanded in the
    /// macro output)
    fn bitfields_type_tokens(&self, context: &HashMap<String, String>) -> 
        TokenStream2 {

        let mut field_tokens = Vec::new();
        let mut field_docstring_tokens = Vec::new();

        for (field_name, field) in &self.fields {
            let field_name_ident = format_ident!("{}", field_name);
            let (field_token, description, default_str) = match &field {
                BitfieldTypes::Bool(bf_def) => (
                    bf_def.type_tokens(context),
                    bf_def.description_string(),
                    bf_def.default_string(),
                    ),
                BitfieldTypes::Uint(bf_def) => (
                    bf_def.type_tokens(context),
                    bf_def.description_string(),
                    bf_def.default_string(),
                    ),
                BitfieldTypes::UintSha1Hash(bf_def) => (
                    bf_def.type_tokens(context),
                    bf_def.description_string(),
                    bf_def.default_string(),
                    ),
            };
            let docstring = format!(
                "{} **[default = `{}`]**", description, default_str);

            field_tokens.push(quote!(#field_name_ident: Option<#field_token>));
            field_docstring_tokens.push(quote!(#[doc = #docstring]));
        }

        let type_name_ident = format_ident!("{}", "Bitfields".to_string());
        quote!(#[derive(Debug, PartialEq, Copy, Clone)]
               pub struct #type_name_ident{#(
                       #field_docstring_tokens
                       pub #field_tokens),*})
    }

    /// Returns the use statement for importing the right bitpacker
    /// bits.
    fn bitpacker_use_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {
        let mut use_strings = HashSet::new();

        for field in self.fields.values() {

            use_strings.insert(match &field {
                BitfieldTypes::Bool(bf_def) => bf_def.use_string(),
                BitfieldTypes::Uint(bf_def) => bf_def.use_string(),
                BitfieldTypes::UintSha1Hash(bf_def) => bf_def.use_string(),
            });
        }

        let field_tokens = use_strings.iter().map(|val| format_ident!("{}", val));
        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");

        let packed_type_ident = format_ident!("{}", packed_type_str);

        quote!(
            use lazy_static::lazy_static;
            use sparrow_bitpacker::{
                #(#field_tokens),*,
                Bitfield,
                BitfieldResult,
                PackedType,
                #packed_type_ident};
        )
    }

    /// Defines the new method. Expands to something like:
    /// ```ignore
    /// pub fn new(go: Option<bool>, stop: Option<bool>,
    ///            length: Option<u32>) -> BitfieldResult<Self> {
    ///
    ///     let go = match go {
    ///         Some(val) => Some(BoolBitfield::<U0>::new(val)?),
    ///         None => None
    ///     };
    /// 
    ///     let stop = match stop {
    ///         Some(val) => Some(BoolBitfield::<U1>::new(val)?),
    ///         None => None
    ///     };
    ///
    ///     let length = match length {
    ///         Some(val) => Some(UintBitfield::<U2, U10>::new(val)?),
    ///         None => None
    ///     };
    ///
    ///     Ok(Bitfields{ go, stop, length })
    /// }
    /// ```
    ///
    /// Internally, it actually uses the alternative syntax:
    /// <BoolBitfield<U0>>::unpack(raw_val)
    fn fn_new_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {

        let mut field_signature_tokens = Vec::new();
        let mut field_assignment_tokens = Vec::new();
        let mut field_return_tokens = Vec::new();

        for (field_name, field) in &self.fields {
            let field_name_ident = format_ident!("{}", field_name);

            let (field_input_type_tokens, field_type_tokens) = match &field {
                BitfieldTypes::Bool(bf_def) => (
                    bf_def.input_type_tokens(context),
                    bf_def.type_tokens(context)),
                BitfieldTypes::Uint(bf_def) => (
                    bf_def.input_type_tokens(context),
                    bf_def.type_tokens(context)),
                BitfieldTypes::UintSha1Hash(bf_def) => (
                    bf_def.input_type_tokens(context),
                    bf_def.type_tokens(context)),
            };

            field_signature_tokens.push(
                quote!(#field_name_ident: Option<#field_input_type_tokens>));

            field_assignment_tokens.push(
                quote!(
                    let #field_name_ident = match #field_name_ident {
                        Some(val) => Some(<#field_type_tokens>::new(val)?),
                        None => None
                    };
                )
            );

            field_return_tokens.push(quote!(#field_name_ident))
        }

        let type_name_ident = format_ident!("{}", "Bitfields".to_string());

        quote!(
            pub fn new(#(#field_signature_tokens),*) -> BitfieldResult<Self> {
                #(#field_assignment_tokens)*

                Ok(#type_name_ident { #(#field_return_tokens),* })
            }
        )

    }

    fn doctest_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {
        let null_namespace = "".to_string();
        let namespace_str = context.get("namespace").unwrap_or(&null_namespace);

        let suffix = "val";

        let mut use_names = Vec::new();
        let mut val_assignment = Vec::new();
        let mut test_assignment = Vec::new();
        let mut field_assignment = Vec::new();

        for (n, (field_name, field)) in (&self.fields).iter().enumerate() {

            let varname = &format!("{}_{}", field_name, suffix);
            let opt_field_random_assignment = match &field {
                BitfieldTypes::Bool(bf_def) =>
                    bf_def.random_assignment_string(varname),
                BitfieldTypes::Uint(bf_def) =>
                    bf_def.random_assignment_string(varname),
                BitfieldTypes::UintSha1Hash(bf_def) =>
                    bf_def.random_assignment_string(varname),
            };

            // The bitfield can opt not to be used in the doctest by returning
            // None to the above function call
            match opt_field_random_assignment {
                Some(field_random_assignment) => {
                    use_names.push(format!("{}", field_name));
                    test_assignment.push(format!("Some({})", varname));

                    let field_assignment_str = if n == 0 {
                        format!("let mut bitfield = {}({}).unwrap();",
                        field_name, varname)
                    } else {
                        format!("bitfield.update(&{}({}).unwrap());",
                        field_name, varname)
                    };

                    val_assignment.push(quote!(
                            #[doc = #field_random_assignment]));
                        field_assignment.push(quote!(
                                #[doc = #field_assignment_str]));
                },
                None => ()
            }
        }

        // We only expand the doctest if we have some bitfields to test
        if !use_names.is_empty() {
            let fields_use_string = format!(
                "use {}::fields::{{{}}};", namespace_str, use_names.join(", "));
            let bitfields_use_string = format!(
                "use {}::{};", namespace_str, "Bitfields");
            let use_tokens = quote!(
                #[doc = #bitfields_use_string]
                #[doc = #fields_use_string]);
            let test_assignment = format!(
                "let test_bf = {}::new({}).unwrap();",
                "Bitfields", test_assignment.join(", "));
            let test_assignment_tokens = quote!(#[doc = #test_assignment]);

            quote!(
                /// ```
                /// use rand::{thread_rng, Rng};
                #use_tokens
                ///
                /// let mut rng = thread_rng();
                ///
                #(#val_assignment)*
                ///
                /// // Normally you'd use `?` rather than `unwrap()`.
                #(#field_assignment)*
                ///
                #test_assignment_tokens
                ///
                /// assert_eq!(bitfield, test_bf);
                /// ```
            )
        } else {
            quote!()
        }
    }

    /// Defines the pack method. Expands to something like:
    /// ```ignore
    /// fn pack(&self) -> BitfieldResult<u32> {
    ///     let mut packed_result = 0;
    ///     packed_result |= match &self.length {
    ///            Some(length) => length.pack()?,
    ///            None => 0,
    ///     };
    ///     packed_result |= match &self.stop {
    ///         Some(stop) => stop.pack()?,
    ///         None => 0,
    ///     };
    ///     packed_result |= match &self.go {
    ///         Some(go) => go.pack()?,
    ///         None => 0,
    ///     };
    ///     Ok(packed_result)
    /// }
    /// ```
    fn fn_pack_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {

        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");
        let packed_type_ident = format_ident!("{}", packed_type_str);

        let mut field_tokens = Vec::new();

        for field_name in self.fields.keys() {
            let field_name_ident = format_ident!("{}", field_name);

            field_tokens.push(quote!(
                    packed_result |= match &self.#field_name_ident {
                        Some(field_val) => field_val.pack()?,
                        None => Bitfields::default().#field_name_ident.expect(
                            "The default bitfield has not been set. \
                            This should never happen and is a serious bug!").pack()?
                    };));
        }

        quote!(
            fn pack(&self) -> BitfieldResult<<#packed_type_ident as PackedType>::Type> {
                let mut packed_result = 0;

                #(#field_tokens)*

                Ok(packed_result)
            }
        )

    }

    /// Defines the unpack method. Expands to something like:
    /// ```ignore
    /// fn unpack(raw_val: u32) -> BitfieldResult<Bitfields> {
    ///     Ok(Bitfields {
    ///         go: Some(BoolBitfield::<U0>::unpack(raw_val)?),
    ///         stop: Some(BoolBitfield::<U1>::unpack(raw_val)?),
    ///         length: Some(UintBitfield::<U2, U10>::unpack(raw_val)?),
    ///     })
    /// }
    /// ```
    ///
    /// Internally, it actually uses the alternative syntax:
    /// <BoolBitfield<U0>>::unpack(raw_val)
    fn fn_unpack_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {

        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");
        let packed_type_ident = format_ident!("{}", packed_type_str);

        let mut field_tokens = Vec::new();

        for (field_name, field) in &self.fields {
            let field_name_ident = format_ident!("{}", field_name);
            let field_type_tokens = match &field {
                BitfieldTypes::Bool(bf_def) => bf_def.type_tokens(context),
                BitfieldTypes::Uint(bf_def) => bf_def.type_tokens(context),
                BitfieldTypes::UintSha1Hash(bf_def) => bf_def.type_tokens(context),
            };

            field_tokens.push(
                quote!(#field_name_ident: Some(<#field_type_tokens>::unpack(raw_val)?))
                );
        }

        let type_name_ident = format_ident!("{}", "Bitfields".to_string());

        quote!(
            fn unpack(raw_val: <#packed_type_ident as PackedType>::Type) -> BitfieldResult<#type_name_ident> {
                Ok(#type_name_ident {
                    #(#field_tokens),*
                })
            }
        )
    }
    
    /// Defines the update method. Expands to something like:
    /// ```ignore
    /// pub fn update(self: &mut Self, other: &Self) {
    ///
    ///     if other.go.is_some() {
    ///         self.go = other.go;
    ///     }
    ///
    ///     if other.stop.is_some() {
    ///         self.stop = other.stop;
    ///     }
    ///
    ///     if other.length.is_some() {
    ///         self.length = other.length;
    ///     }
    /// }
    /// ```
    fn fn_update_tokens(&self, _context: &HashMap<String, String>) -> TokenStream2 {
        let mut field_tokens = Vec::new();

        for field_name in self.fields.keys() {
            let field_name_ident = format_ident!("{}", field_name);

            field_tokens.push(quote!(
                    if other.#field_name_ident.is_some() {
                        self.#field_name_ident = other.#field_name_ident;
                    }));
        }

        quote!(
            pub fn update(self: &mut Self, other: &Self) {
                #(#field_tokens)*
            }
        )

    }

    /// Sets the BITFIELD_DEFAULTS constant. Creates something that acts
    /// a bit like:
    /// ```ignore
    /// lazy_static! {
    ///     static ref BITFIELD_DEFAULTS: #type_name_ident = {
    ///         match #type_name_ident::new(#field_assignment) {
    ///            Ok(defaults) => defaults,
    ///            Err(e) => panic!(
    ///                "Error setting defaults: {}. \
    ///                            This should have been detected at \
    ///                            compile time and is a bug. Please report it.", e)
    ///         }
    ///     };
    /// }
    /// ```
    /// It uses lazy_static!() macro because we still want to use the creation 
    /// machinery to check that values are acceptable. We also put a compile
    /// time check before run time as well. The latter should protect against
    /// the former failing, but there is a potential mismatch between the
    /// type that is used here and the type that is used at runtime, so this
    /// way we are doubly sure.
    fn bitfield_defaults_tokens(&self, _context: &HashMap<String, String>) -> TokenStream2 {
        let type_name_ident = format_ident!("{}", "Bitfields".to_string());
        let mut field_assignment = Vec::new();

        for (_field_name, field) in &self.fields {

            let field_default_tokens = match &field {
                BitfieldTypes::Bool(bf_def) => bf_def.default_tokens(),
                BitfieldTypes::Uint(bf_def) => bf_def.default_tokens(),
                BitfieldTypes::UintSha1Hash(bf_def) => bf_def.default_tokens(),
            };

            field_assignment.push(quote!(Some(#field_default_tokens)));
        }

        let field_assignment = quote!(#(#field_assignment),*);

        quote!(

            lazy_static! {
                /// Holds the default values for the bitfields. This can be
                /// checked to see what any unset values will look like.
                static ref BITFIELD_DEFAULTS: #type_name_ident = {
                    match #type_name_ident::new(#field_assignment) {
                        Ok(defaults) => defaults,
                        Err(e) => panic!(
                            "Error setting defaults: {}. \
                        This should have been detected at \
                        compile time and is a bug. Please report it.", e)
                    }
                };
            }
        )

    }

    /// Declares the multi-bitfield type. Expands to something like:
    /// ```ignore
    /// #[derive(Debug, PartialEq, Copy, Clone)]
    /// pub struct ExtractedBitfields {
    ///     go: bool,
    ///     stop: bool,
    ///     length: u32,
    /// }
    /// ```
    /// (though of course the derive attributes are further expanded in the
    /// macro output)
    fn extracted_bitfields_type_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {

        let mut field_tokens = Vec::new();

        for (field_name, field) in &self.fields {
            let field_name_ident = format_ident!("{}", field_name);
            let field_token = match &field {
                BitfieldTypes::Bool(bf_def) => bf_def.input_type_tokens(context),
                BitfieldTypes::Uint(bf_def) => bf_def.input_type_tokens(context),
                BitfieldTypes::UintSha1Hash(bf_def) => bf_def.input_type_tokens(context),
            };

            field_tokens.push(quote!(#field_name_ident: #field_token));
        }

        let type_name_ident = format_ident!("Extracted{}", "Bitfields".to_string());
        quote!(#[derive(Debug, PartialEq, Copy, Clone)]
               pub struct #type_name_ident{#(pub #field_tokens),*})
    }


    /// Implements the from method for the ExtractedX type
    /// (intended to be whilst implementing the `From` trait).
    ///
    /// Expands to something like:
    /// ```ignore
    /// fn from(bitfield: Bitfields) -> Self {
    ///
    ///     let go = match bitfield.go {
    ///         Some(val) => val.into_inner(),
    ///         None => Bitfields::default().go.expect("err string").into_inner()
    ///     };
    ///
    ///     let stop = match bitfield.stop {
    ///         Some(val) => val.into_inner(),
    ///         None => Bitfields::default().stop.expect("err string").into_inner()
    ///     };
    ///
    ///     let length = match bitfield.length {
    ///         Some(val) => val.into_inner(),
    ///         None => Bitfields::default().length.expect("err string").into_inner()
    ///     };
    ///
    ///     Self { go, stop, length, }
    /// }
    /// ```
    fn fn_extracted_from_tokens(&self, _context: &HashMap<String, String>) -> TokenStream2 {

        let mut field_tokens = Vec::new();
        let mut field_return_tokens = Vec::new();

        for (field_name, _field) in &self.fields {
            let field_name_ident = format_ident!("{}", field_name);

            field_tokens.push(quote!(
                    let #field_name_ident = match bitfield.#field_name_ident {
                        Some(val) => val.into_inner(),
                        None => Bitfields::default().#field_name_ident.expect(
                            "The default bitfield has not been set. \
                            This should never happen and is a serious bug!").into_inner()
                    };)
            );

            field_return_tokens.push(quote!(#field_name_ident))
        }

        let type_name_ident = format_ident!("{}", "Bitfields".to_string());
        quote!(
            fn from(bitfield: #type_name_ident) -> Self {

                #(#field_tokens)*

                Self { #(#field_return_tokens),* }
            }
        )

    }

    fn mod_field_constructors_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {
        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");

        let mut field_tokens = Vec::new();
        let type_name_ident = format_ident!("{}", "Bitfields".to_string());
        let packed_type_ident = format_ident!("{}", packed_type_str);

        for (n, (field_name, field)) in (&self.fields).iter().enumerate() {
            let field_name_ident = format_ident!("{}", field_name);

            let mut field_assignment = Vec::new();

            for k in 0..self.fields.len() {
                if k == n {
                    field_assignment.push(quote!(Some(val)));
                } else {
                    field_assignment.push(quote!(None));
                }
            }

            let field_assignment = quote!(#(#field_assignment),*);

            let field_input_type_token = match &field {
                BitfieldTypes::Bool(bf_def) => bf_def.input_type_tokens(context),
                BitfieldTypes::Uint(bf_def) => bf_def.input_type_tokens(context),
                BitfieldTypes::UintSha1Hash(bf_def) => bf_def.input_type_tokens(context),
            };

            let field_doc_string = format!(
                "Returns a bitfield with only the `{}` field set.",
                field_name);

            field_tokens.push(
                quote!(
                    #[doc = #field_doc_string]
                    pub fn #field_name_ident(
                        val: #field_input_type_token) -> BitfieldResult<#type_name_ident> {
                        #type_name_ident::new(#field_assignment)
                    }
                )
            );
        }
        quote!(
            pub mod fields {
                use super::#type_name_ident;
                use sparrow_bitpacker::{
                    BitfieldResult, PackedType, #packed_type_ident};

                #(#field_tokens)*
            }
        )

    }

    /// Creates any extra useful functions that the bitfield needs.
    fn extra_bitfield_methods_tokens(&self, context: &HashMap<String, String>)
        -> TokenStream2
    {
        let mut bitfield_methods_tokens = Vec::new();

        for (field_name, field) in &self.fields {
            let mut new_context = context.clone();
            new_context.insert("field_name".to_string(), field_name.to_string());

            let opt_methods_tokens = match &field {
                BitfieldTypes::Bool(bf_def) =>
                    bf_def.extra_bitfield_methods(&new_context),
                BitfieldTypes::Uint(bf_def) =>
                    bf_def.extra_bitfield_methods(&new_context),
                BitfieldTypes::UintSha1Hash(bf_def) =>
                    bf_def.extra_bitfield_methods(&new_context),
            };

            bitfield_methods_tokens.push(opt_methods_tokens.unwrap_or_default());
        }
        quote!(
            #(#bitfield_methods_tokens)*
        )
    }

    /// The context provides information about things this bitfield needs to
    /// know about. Not all context values are acceptable for all bitfields.
    /// The method checks the context is ok for each bitfield.
    /// e.g. a UintSha1Hash bitfield expects the register to be read-only.
    /// (note, the purpose of this is function is not to check the context
    /// contains relevant values - which is done as needed - but to check
    /// that the context is _correct_ as necessary)
    fn check_context(&self, context: &HashMap<String, String>) {
        let reg_type = context.get("register_type").expect(
            "Missing register_type in the context");

        for (field_name, field) in &self.fields {
            let reg_type_ok = match &field {
                BitfieldTypes::Bool(bf_def) =>
                    bf_def.register_type_check(reg_type),
                BitfieldTypes::Uint(bf_def) =>
                    bf_def.register_type_check(reg_type),
                BitfieldTypes::UintSha1Hash(bf_def) =>
                    bf_def.register_type_check(reg_type),
            };

            match reg_type_ok {
                true => (),
                false => panic!(
                    "The register type `{}`, is invalid for bitfield `{}`",
                    reg_type, field_name)
            }
        }
    }

    /// Checks two requirements: That the bitfields do not overflow the 
    /// register and that the bitfields don't overlap with one another.
    /// The 2nd is a policy decision that could be changed, but we expect that
    /// the risk of setting the bitfields wrong is greater than the desire
    /// to have actually overlapping bitfields (which might be interesting for
    /// e.g. multiple views into the same register)
    fn check_bitfields_valid(&self, context: &HashMap<String, String>) {

        let register_length_str = context.get("register_length").expect(
            "Missing register_length in the context");

        let register_length = register_length_str.parse::<usize>().expect(
            "For some reason the register length cannot be parsed as a usize");

        let mut checked_bit_positions: HashMap<&str, HashSet<_>> = HashMap::new();

        for (field_name, field) in &self.fields {
            // This strategy is agnostic to the actual layout, and doesn't
            // have any knowledge about the underlying data type. It simply
            // looks at populated bit indices.
            let bit_positions = match &field {
                BitfieldTypes::Bool(bf_def) => bf_def.bit_positions(),
                BitfieldTypes::Uint(bf_def) => bf_def.bit_positions(),
                BitfieldTypes::UintSha1Hash(bf_def) => bf_def.bit_positions(),
            };

            for bit_position in &bit_positions {

                if bit_position >= &register_length {
                    panic!(
                        "The bitfield settings for bitfield \"{}\" will cause \
                        the register to overflow given the packed size ({}).",
                        field_name, register_length_str);
                }
            }
            // We check the new bit_positions we've just got against the
            // previous bit_positions we've already checked.
            for (other_field_name, other_positions) in &checked_bit_positions {
                let overlap: Vec<_> =
                    other_positions.intersection(&bit_positions).collect();

                if !overlap.is_empty() {
                    panic!(
                        "The bitfields {} and {} are overlapping. \
                        This is not allowed.", other_field_name, field_name);
                }
            }

            // Finally we need to add the new positions to the hashmap of
            // checked positions.
            checked_bit_positions.insert(field_name, bit_positions);
        }
    }
}


impl ToTokens for MultiBitfieldDef {

    /// Returns the tokens that represent the bitfields.
    fn to_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {

        // Firstly, do some checks
        // Make sure the context looks ok
        self.check_context(context);
        // Check the bitfields are valid
        self.check_bitfields_valid(context);

        let bitfields_type_declaration =  self.bitfields_type_tokens(context);
        let bitpacker_use = self.bitpacker_use_tokens(context);
        let fn_new = self.fn_new_tokens(context);
        let main_doctest = self.doctest_tokens(context);
        let fn_pack = self.fn_pack_tokens(context);
        let fn_unpack = self.fn_unpack_tokens(context);
        let fn_update = self.fn_update_tokens(context);
        let bitfield_defaults = self.bitfield_defaults_tokens(context);
        let extracted_bitfields_type_declaration =
            self.extracted_bitfields_type_tokens(context);
        let fn_extracted_from = self.fn_extracted_from_tokens(context);
        let mod_field_constructors = self.mod_field_constructors_tokens(context);
        let extra_bitfield_methods = self.extra_bitfield_methods_tokens(context);

        let type_name_ident = format_ident!("{}", "Bitfields".to_string());
        let extracted_type_name_ident = format_ident!(
            "Extracted{}", "Bitfields".to_string());

        let extracted_bitfields_docstring = format!(
            "A helpful type for easily getting \
            access to the internal values of a [`{type_name}`] object. Its \
            fields have more useful low-level types for further processing.\n
            \n\
            The [`From`] trait is implemented \
            (which implicitly provides [`Into`] for [`{type_name}`]).",
            type_name = type_name_ident.to_string());

        let extracted_bitfields_from_docstring = format!(
            "Creates an [`{extracted_type_name}`] from a [`{type_name}`], in \
            which the types are as expected for downstream usage.\n\
            \n\
            Current policy is for a `None` in `{type_name}` to map to its \
            equivalent \"zero\" value (i.e. 0 or false).",
            type_name = type_name_ident.to_string(),
            extracted_type_name = extracted_type_name_ident.to_string());

        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");
        let packed_type_ident = format_ident!("{}", packed_type_str);

        quote!(
            use std::default::Default;
            #bitpacker_use

            #bitfield_defaults

            ///
            /// The type is use to construct and manipulate bitfields in a
            /// safe and consistent way.
            ///
            /// It might be easiest to construct the bitfield with suitable calls
            /// to functions in [`fields`].
            #main_doctest
            #bitfields_type_declaration

            impl Bitfield<#packed_type_ident> for #type_name_ident {

                #fn_pack

                #fn_unpack
            }

            impl Default for #type_name_ident {

                fn default() -> Self {
                    *BITFIELD_DEFAULTS
                }
            }

            impl #type_name_ident {

                /// Creates a new bitfield from the provided optional
                /// arguments.
                ///
                #fn_new

                /// Updates the bitfield with values extracted from `other`.
                ///
                /// Only fields in `other` that are not `None` will result
                /// in those fields being updated.
                #fn_update

                #extra_bitfield_methods
            }

            #[doc = #extracted_bitfields_docstring]
            ///
            /// The expected use-case is in reading a register. Something like
            /// ```ignore
            /// if register.read()?.into().a_bool_field {
            ///     // ... do something
            /// }
            /// ```
            #extracted_bitfields_type_declaration

            impl From<#type_name_ident> for #extracted_type_name_ident {

                #[doc = #extracted_bitfields_from_docstring]
                #fn_extracted_from

            }

            /// A module containing a constructor for each individual field.
            #mod_field_constructors

        )
    }
}

