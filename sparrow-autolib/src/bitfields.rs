use proc_macro2::TokenStream as TokenStream2;
use proc_macro2::Span;
use quote::{quote, format_ident};
use std::collections::{HashMap, HashSet};
use std::convert::TryInto;

use sparrow_map_config::{
    bitfields::{BoolBitfieldDef, UintBitfieldDef,  UintSha1HashBitfieldDef}};

pub(crate) trait BitfieldTokenOutputs {

    fn type_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn use_string(&self) -> String;
    fn input_type_tokens(&self, context: &HashMap<String, String>) -> TokenStream2;
    fn default_tokens(&self) -> TokenStream2;
    fn default_string(&self) -> String;
    fn description_string(&self) -> String;
    fn random_assignment_string(&self, var_name: &str) -> Option<String>;
    fn equivalent_packing_string(&self, var_name: &str) -> String;
    fn register_type_check(&self, register_type: &str) -> bool;
    fn extra_bitfield_methods(&self, context: &HashMap<String, String>)
        -> Option<TokenStream2>;
    fn bit_positions(&self) -> HashSet<usize>;
}

impl BitfieldTokenOutputs for BoolBitfieldDef {

    fn type_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {
        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");
        let offset_ident = format_ident!("U{}", self.offset);
        let packed_type_ident = format_ident!("{}", packed_type_str);
        quote!(BoolBitfield<#offset_ident, #packed_type_ident>)
    }

    fn use_string(&self) -> String {
        "BoolBitfield".to_string()
    }

    fn input_type_tokens(&self, _context: &HashMap<String, String>) -> TokenStream2 {
        quote!(bool)
    }

    fn default_tokens(&self) -> TokenStream2 {

        match self.default {
            Some(default) => {
                if default {
                    quote!(true)
                } else {
                    quote!(false)
                }},
            None => quote!(false)
        }
    }

    fn default_string(&self) -> String {

        match self.default {
            Some(default) => {
                if default {
                    format!("{}", default).to_string()
                } else {
                    format!("{}", default).to_string()
                }},
            None => format!("{}", false).to_string()
        }
    }

    fn description_string(&self) -> String {
        match &self.description {
            Some(description) => description.clone(),
            None => "No description".to_string()
        }
    }

    fn random_assignment_string(&self, var_name: &str) -> Option<String> {
        Some(format!("let {}: bool = rng.gen();", var_name))
    }

    fn equivalent_packing_string(&self, var_name: &str) -> String {

        format!("({} as u32) << {}", var_name, self.offset)
    }

    fn register_type_check(&self, _register_type: &str) -> bool {
        true
    }

    fn extra_bitfield_methods(
        &self, _context: &HashMap<String, String>) -> Option<TokenStream2>
    {
        None
    }

    fn bit_positions(&self) -> HashSet<usize> {
        let mut bit_positions = HashSet::new();
        bit_positions.insert(self.offset as usize);

        bit_positions
    }
}

impl BitfieldTokenOutputs for UintBitfieldDef {

    fn type_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {
        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");

        let offset_ident = format_ident!("U{}", self.offset);
        let length_ident = format_ident!("U{}", self.length);
        let packed_type_ident = format_ident!("{}", packed_type_str);

        quote!(UintBitfield<#offset_ident, #length_ident, #packed_type_ident>)
    }

    fn use_string(&self) -> String {
        "UintBitfield".to_string()
    }

    fn input_type_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {
        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");
        let packed_type_ident = format_ident!("{}", packed_type_str);

        quote!(<#packed_type_ident as PackedType>::Type)
    }

    fn default_tokens(&self) -> TokenStream2 {

        let default = match self.default {
            Some(default) => default,
            None => 0
        };

        let lit_default = syn::LitInt::new(
            &*format!("{}", default), Span::call_site());

        quote!(#lit_default)
    }

    fn default_string(&self) -> String {

        match self.default {
            Some(default) => format!("{}", default).to_string(),
            None => format!("{}", 0).to_string()
        }
    }

    fn description_string(&self) -> String {
        match &self.description {
            Some(description) => description.clone(),
            None => "No description".to_string()
        }
    }

    fn random_assignment_string(&self, var_name: &str) -> Option<String> {

        if self.length < 32 {
            Some(format!(
                    "let {}: u32 = rng.gen_range(0, 2u32.pow({}));",
                    var_name, self.length))
        } else {
            Some(format!(
                "let {}: u32 = rng.gen();", var_name))
        }
    }

    fn equivalent_packing_string(&self, var_name: &str) -> String {

        format!("{} << {}", var_name, self.offset)
    }

    fn register_type_check(&self, _register_type: &str) -> bool {
        true
    }

    fn extra_bitfield_methods(
        &self, _context: &HashMap<String, String>) -> Option<TokenStream2>
    {
        None
    }

    fn bit_positions(&self) -> HashSet<usize> {
        let start = self.offset as usize;
        let end = self.offset as usize + self.length as usize;

        (start..end).collect()
    }
}

impl BitfieldTokenOutputs for UintSha1HashBitfieldDef {

    fn type_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {
        self.uint_bitfield.type_tokens(context)
    }

    fn use_string(&self) -> String {
        self.uint_bitfield.use_string()
    }

    fn input_type_tokens(&self, context: &HashMap<String, String>) -> TokenStream2 {
        self.uint_bitfield.input_type_tokens(context)
    }

    fn default_tokens(&self) -> TokenStream2 {
        self.uint_bitfield.default_tokens()
    }

    fn default_string(&self) -> String {
        self.uint_bitfield.default_string()
    }

    fn description_string(&self) -> String {
        self.uint_bitfield.description_string()
    }

    fn random_assignment_string(&self, _var_name: &str) -> Option<String> {
        None
    }

    fn equivalent_packing_string(&self, var_name: &str) -> String {
        self.uint_bitfield.equivalent_packing_string(var_name)
    }

    fn register_type_check(&self, register_type: &str) -> bool {
        match register_type {
            "RO" => true,
            _ => false
        }
    }

    fn extra_bitfield_methods(
        &self, context: &HashMap<String, String>) -> Option<TokenStream2>
    {
        let packed_type_str = context.get("packed_type").expect(
            "Missing packed_type in the context");

        let field_name = context.get("field_name").expect(
            "Missing field_name in the context");

        let yaml_sha1_hash = context.get("yaml_sha1_hash").expect(
            "Missing yaml_sha1_hash in the context");

        let docstring = format!(
            "Verify the bitfield `{}` against the expected SHA1 hash value.",
            field_name);

        let fn_name = format_ident!("verify_{}", field_name);
        let field_name_ident = format_ident!("{}", field_name);
        let packed_type_ident = format_ident!("{}", packed_type_str);

        let mut digits = Vec::new();
        for digit in yaml_sha1_hash.chars() {
            digits.push(digit.to_digit(16).unwrap());
        }

        const DIGIT_SIZE: u32 = 4;

        let mut expected_hash = 0;
        let digest_digits: usize = self.digest_digits.try_into().expect(
            "digest_digits cannot fit inside a usize, which is very odd. This is a bug.");
        let digest_offset: usize = self.digest_offset.try_into().expect(
            "digest_digits cannot fit inside a usize, which is very odd. This is a bug.");

        for n in digest_offset..(digest_offset + digest_digits) {
            expected_hash = (expected_hash << DIGIT_SIZE) | digits[n];
        }

        Some(quote!(
            #[doc = #docstring]
            /// Returns `true` if the bitfield is correct.
            pub fn #fn_name(&self) -> bool {
                const EXPECTED_SHA1_HASH: <#packed_type_ident as PackedType>::Type = 
                    #expected_hash as <#packed_type_ident as PackedType>::Type;

                let sha1_hash: <#packed_type_ident as PackedType>::Type =
                    self.#field_name_ident.unwrap_or_default().into_inner();

                sha1_hash == EXPECTED_SHA1_HASH
            }
        ))
    }

    fn bit_positions(&self) -> HashSet<usize> {
        self.uint_bitfield.bit_positions()
    }

}
