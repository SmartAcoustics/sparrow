use trybuild;
use glob::glob;
use regex::{Regex, NoExpand};
use std::{
    fs::File,
    io::{BufWriter, Write},
    path::PathBuf};

use std;

fn setup_failing_rust_file(yaml_file: String, packed_type: Option<String>) {

    let yaml_file_path = PathBuf::from(yaml_file);

    let mut rust_file_path = yaml_file_path.clone();
    rust_file_path.set_extension("rs");

    let rs_write_file = File::create(&rust_file_path).unwrap();
    let mut rs_writer = BufWriter::new(&rs_write_file);

    let macro_call_str = match packed_type {
        None => {
            format!(
                "build_register_lib!(\"{}\");",
                yaml_file_path.canonicalize().unwrap().to_str().unwrap())},
        Some(packed_type_str) => {
            format!(
                "build_register_lib!(\"{}\", \"{}\");",
                yaml_file_path.canonicalize().unwrap().to_str().unwrap(),
                packed_type_str)},
    };

    write!(
        &mut rs_writer,
        "// THIS FILE IS CREATED AUTOMATICALLY. EDITS WILL BE OVERWRITTEN. \
        Read the README file on how to add new failing cases.\n\
        #![allow(unused_imports)] use sparrow_autolib::build_register_lib;\
        use sparrow_raw_peripherals::RawPeripheral;\n{}\n\
        fn main() {{}}", macro_call_str
    ).unwrap();

    // We also have to modify the stderr file
    let mut stderr_file_path = yaml_file_path.clone();
    stderr_file_path.set_extension("stderr");

    let stderr_filename = stderr_file_path.file_name().unwrap();
    // We construct the template filename as the stderr file but in
    // the directory "stderr_templates".
    let mut stderr_template_file_path = stderr_file_path.clone();
    stderr_template_file_path.pop();
    stderr_template_file_path.push("stderr_templates");
    stderr_template_file_path.push(stderr_filename);


    // On file error, we ignore it. It implies the file is missing, which
    // means we don't write the populated template, which triggers the
    // wip behaviour of trybuild, which is what we want.
    match std::fs::read_to_string(stderr_template_file_path){
        Ok(err_template) => {
            // Find the line that looks like "  | ^^^^^^^^^", pointing out the
            // error location.
            let re = Regex::new(r"(?m)^  \| \^*$").unwrap();
            let expected_string = format!(
                "  | {}", "^".repeat(macro_call_str.chars().count() - 1));
            let modified_err = re.replace(&err_template, NoExpand(&expected_string));

            match File::create(&stderr_file_path) {
                Ok(stderr_write_file) => {
                    let mut stderr_writer = BufWriter::new(&stderr_write_file);
                    write!(&mut stderr_writer, "{}", modified_err).unwrap();
                },
                Err(_) => panic!("Cannot write populated template")
            }
        },
        Err(_) => ()
    }
}

#[test]
fn failing_cases() {

    // Need to create the files here using an absolute path. There are
    // issues with the way trybuild creates a new crate.
    for each_file in glob("tests/yaml_fails/*.yaml").unwrap() {
        setup_failing_rust_file(
            format!("{}", each_file.unwrap().display()), None)
    }

    let t = trybuild::TestCases::new();
    t.compile_fail("tests/yaml_fails/*.rs");

    // Now setup and test the usage fails

    // If we pass in an invalid packed type, we should fail sensibly
    setup_failing_rust_file(
        "tests/usage_fails/invalid_packed_type.yaml".to_string(),
        Some("DSFSFDF".to_string()));

    let t = trybuild::TestCases::new();
    t.compile_fail("tests/usage_fails/*.rs");
}
